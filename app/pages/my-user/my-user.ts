import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the MyUserPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/my-user/my-user.html',
})
export class MyUserPage {

  constructor(private navCtrl: NavController) {

  }

}
