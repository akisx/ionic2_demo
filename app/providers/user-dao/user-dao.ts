import { Injectable } from '@angular/core';
import { Storage, SqlStorage } from 'ionic-angular';
import 'rxjs/add/operator/map';

import {MyUser} from '../../models/MyUser'
/*
  Generated class for the UserDao provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserDao {
  storage:Storage;

  constructor() {
    this.storage = new Storage(SqlStorage);
    this.storage
    .query(
      "CREATE TABLE IF NOT EXISTS my_user (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)"
      );
  }

  add(user: MyUser){
    this.storage.query(`insert into my_user(name) values(${user.name})`);
  }

  get(){
    return this.storage.query(`select * from my_user`);
  }

}

